var navbar = document.getElementById("navbar");
var navbar_right = document.getElementById("navbar_right");
var navbar_left = document.getElementById("navbar_left");

window.onscroll = function () {
    var currentScrollPos = window.pageYOffset;
    if (currentScrollPos < 900) {
        navbar.style.top = "0";
        navbar_right.style.marginTop = "0px";
        navbar_left.style.transform = "scale(1)";
        navbar_left.style.marginTop = "0px";

    } else {
        navbar.style.top = "-20px";
        navbar_right.style.marginTop = "9px";
        navbar_left.style.transform = "scale(0.6)";
        navbar_left.style.marginTop = "10px";
    }
}