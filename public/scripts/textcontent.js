


// document.getElementById('exampleTextParagraph').textContent = "\
// ";

document.getElementById('o_nas_1').textContent = "\
    Koło Naukowe Twórców Gier Polygon działa na Politechnice Warszawskiej od 2009 roku i tworzy bogatą społeczność, \
    w której skład wchodzi wielu pasjonatów tworzenia gier, a także i profesjonalistów w tej dziedzinie. \
	";

document.getElementById('o_nas_2').textContent = "\
    Koło zostało założone przez grupę pasjonatów, którzy chcieli wspólnie wymieniać się wiedzą oraz doświadczeniem, a także wspierać się w tworzeniu własnych projektów. \
    Polygon organizuje cotygodniowe, otwarte dla wszystkich spotkania, przybierające formę wykładów o tematyce ściśle związanej z najróżniejszymi aspektami procesu tworzenia gier.\
    Stopniowo stawały się one coraz bardziej popularne i zaczęły przyciągać nie tylko weteranów, ale także osoby dopiero pragnące rozpocząć swoją przygodę z gamedevem. \
    Początkowo wykłady prowadzone były przez członków Koła, z czasem jednak zaczęły pojawiać się także osoby z \"z zewnątrz\", chcące się nim podzielić. \
	";

document.getElementById('o_nas_3').textContent = "\
    Oprócz zwykłej działalności, członkowie Koła prowadzą prelekcje i warsztaty na imprezach takich jak Game Industry Conference, Pyrkon, PixelHeaven, czy CodePot. \
    ";

document.getElementById('o_nas_4').textContent = "\
    Największymi wydarzeniami organizowanymi przez Polygon są Slavic Game Jam – jeden z największych game jamów w Polsce, \
    PolyJam – warszawska edycja Global Game Jamu, oraz Game Dev Fest – cykl spotkań ze znanymi ludźmi ze świata gamedevu. \
    Dodatkowo Kołowicze o podobnych zainteresowaniach łączą się kręgi, czyli grupy osób wspólnie szlifujących umiejętności w konkretnym obszarze. \
	Jednak głównym celem Polygonu od początku było tworzenie zżytej społeczności, a także pomaganie osobom chcącym wejść do świata gamedevu. \
    ";

document.getElementById('spotkania_1').textContent = "\
    Obecnie na każdym ze spotkań pojawiają się dziesiątki osób pragnących poszerzyć swoją wiedzę lub po prostu spotkać się i pogadać ze swoimi znajomymi z Koła. \
    Wykłady dostępne są dla każdego – od początkujących, do ludzi pracujących już w branży. Nie trzeba być członkiem Koła, aby brać udział w spotkaniach. \
    Miejsca spokojnie wystarcza dla wszystkich, gdyż gromadzimy się w dużej auli na Wydziale Elektroniki i Technik Informacyjnych. \
    Zgodnie z wieloletnią tradycją, termin spotkań jest zawsze ten sam – środa, godzina 19:00, a ich tematyka obejmuje wszystko, \
    co wiąże się bezpośrednio lub pośrednio z tworzeniem gier – programowanie, grafikę 3D i 2D, projektowanie, dźwięk, czy promocję.\
    \r\n \r\n \
    Wykłady prowadzą nie tylko osoby należące do Koła, ale też przedstawiciele firm takich jak CD Projekt RED, Techland, czy 11 bit studios, \
    którzy chętnie dzielą się swoim wieloletnim doświadczeniem w tworzeniu większych produkcji. \
    Zwyczajowo, na każdym spotkaniu ma miejsce również PolygonHype, czyli chwila podczas której, każdy ma możliwość zaprezentowania czegoś, \
    nad czym ostatnio pracował i chciałby się tym pochwalić. \
    ";

document.getElementById('inicjatywy_SlavicGameJam').textContent = "\
    Jeden z największych game jamów nie tylko w Polsce, ale również w Europie Środkowo-Wschodniej. \
    W ramach imprezy, oprócz głównej części, czyli 48-godzinnego maratonu tworzenia gier, odbywają się również wykłady, warsztaty, imprezy integracyjne, \
    a także szereg innych aktywności. \
    Slavic Game Jam jest organizowany w całości w języku angielskim, tak aby dać możliwość udziału jak największej ilości uczestników z innych krajów. \
    ";

document.getElementById('inicjatywy_PolyJam').textContent = "\
    Warszawska edycja (tzw. site) międzynarodowego Global Game Jamu organizowana dla wszystkich pasjonatów gamedevu. \
    Głównym celem wydarzenia jest integracja i dobra zabawa podczas 48-godzinnej pracy nad grami. \
    Podobnie jak Slavic Game Jam, PolyJam jest idealnym wydarzeniem dla osób, które jeszcze nigdy nie brały udziału w game jamach, a chciałyby spróbować. \
    ";

document.getElementById('inicjatywy_Piwo').textContent = "\
    Jedna z najważniejszych inicjatyw Koła. To właśnie na \"Polygonowym Piwie\" można zintegrować się z innymi członkami koła i zawrzeć znajomości na całe życie, \
    pozyskać feedback, zasięgnąć rady doświadczonych game developerów, poszukać pracy, czy po prostu miło spędzić czas. (Pssst na \"Piwie\" nie trzeba pić piwa)\
    ";

document.getElementById('inicjatywy_Projekty').textContent = "\
    Ważną częścią zadań Koła jest również umożliwienie postawienia swoich pierwszych kroków w tworzeniu gier osobom, które jeszcze tego nie robiły i nie mają żadnego doświadczenia. \
    W ramach semestralnych projektów, nowi członkowie Koła mogą dołączyć do kilkuosobowych drużyn, z których każda ma przydzielonego mentora, którego zadaniem jest jej pomagać. \
    Celem drużyny jest oczywiście zrobienie prostej gry i zaprezentowanie efektów pracy pod koniec semestru. \
    ";

document.getElementById('inicjatywy_GameDevFest').textContent = "\
    Game Dev Fest to cykl czterech spotkań z weteranami polskiej branży tworzenia gier komputerowych. \
    Jest to świetna okazja do wysłuchania doświadczonych game developerów i skorzystania z ich rad. \
    Impreza jest przeznaczona zarówno dla początkujących twórców, jak i wyjadaczy z branży. \
    Każdy znajdzie coś dla siebie. Wstęp na spotkania jest całkowicie bezpłatny. \
    ";

document.getElementById('inicjatywy_Kręgi').textContent = "\
    Kręgi są „kołami w kole”. Służą zdobywaniu specjalistycznej wiedzy, związanej z konkretną dziedziną. W ramach kręgów \
    często organizowane są dodatkowe spotkania, wykłady, warsztaty – zupełnie niezależnie od spotkań całego koła. \
    Przykładami kręgów są kregi: Unity, Unreal'a, grafiki, VFX, robienia dobrych gier. \
    Każdym kręgiem zarządza mistrz i to on decyduje o wszystkim: m.in. o tym, jak dołączyć do kręgu, czym krąg się zajmuje, i w jaki sposób działa. \
    ";

document.getElementById('inicjatywy_Wyjazdy').textContent = "\
    Koło organizuje również liczne wyjazdy na wszelkiego rodzaju wydarzenia związane z gamedevem. Są to przede wszystkim konferencje takie jak \
    Game Developers Conference, Digital Dragons, Game Industry Conference i Inżynieria Gier Komputerowych, bądź game jamy takie jak TK Game Jam, Warsaw Film School Game Jam, \
    Sensei Game Jam, PGG Jam, Gryf Game Jam, czy Castle Game Jam. \
    ";